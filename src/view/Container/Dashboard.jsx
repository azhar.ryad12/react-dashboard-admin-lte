import { Navbar, Sidebar, Footer } from '../Component'
import { DashboardContent } from '../Content'

const Dashboard = () => {
    return (
        <>
            <Navbar />
            <Sidebar />
            <DashboardContent />
            <Footer />
        </>
    )
}

export default Dashboard