import {Routes, Route} from 'react-router-dom'

// import pages
import { Container } from '../../view'


function Routers() {
  return (
    <div>
      <Routes>        
        <Route path='/' element={<Container />} exact />        
      </Routes>
    </div>
  );
}

export default Routers;