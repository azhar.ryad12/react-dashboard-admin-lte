const INITIAL_STATE = {
    page: 'dashboard',   
}

const dashboardReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'DASHBOARD':
            return {
                ...state,
                neworder: action.payload
            }        
        default:
            return state
    }
}

export default dashboardReducer