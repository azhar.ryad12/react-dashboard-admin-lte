import React from 'react'

import Routers from './application/router'

function App() {
  return (
    <Routers />
  );
}

export default App;
